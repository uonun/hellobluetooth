package com.test.helloBluetooth;

import android.os.Bundle;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import com.test.helloworld.R;

public class MainActivity extends Activity {

	// 该UUID表示串口服务, http://wiley.iteye.com/blog/1179417
	static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";
	static final String MAC_UOPC = "EC:55:F9:DA:E6:16";
	static final String MAC_JIMMY = "44:A7:CF:27:E1:79";

	BluetoothReceiver bluetoothReceiver = null;
	TextView txt = null;
	Button buMatch = null;

	// 得到BluetoothAdapter对象
	final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	BluetoothDevice foundDevice = null;
	BluetoothDevice remoteDevice = null;
	BluetoothSocket btSocket = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button buEnum = (Button) findViewById(R.id.button1);
		Button buSearch = (Button) findViewById(R.id.button3);
		buMatch = (Button) findViewById(R.id.button2);
		txt = (TextView) findViewById(R.id.textView1);

		buMatch.setEnabled(false);

		buEnum.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				txt.append("================================\r\n");
				if (adapter != null) {
					txt.append("找到蓝牙设备\r\n");
					// 调用isEnabled()方法判断当前蓝牙设备是否可用
					if (!adapter.isEnabled()) {
						txt.append("蓝牙未打开，正在打开...\r\n");
						adapter.enable();
						// 如果蓝牙设备不可用的话,创建一个intent对象,该对象用于启动一个Activity,提示用户启动蓝牙适配器
						// Intent intent = new
						// Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
						// startActivity(intent);

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

					if (adapter.isEnabled()) {
						txt.append("成功打开蓝牙...\r\n");
						txt.append("本机蓝牙地址：" + adapter.getAddress() + "\r\n");

						// 提示是否允许其他蓝牙设备发现当前设备
						// Intent discoverableIntent = new Intent(
						// BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
						// discoverableIntent.putExtra(
						// BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
						// startActivity(discoverableIntent);

						// 得到所有已经配对的蓝牙适配器对象
						Set<BluetoothDevice> devices = adapter
								.getBondedDevices();
						if (devices.size() > 0) {
							txt.append("开始枚举已配对的蓝牙设备...\r\n");
							// 用迭代
							for (Iterator iterator = devices.iterator(); iterator
									.hasNext();) {
								// 得到BluetoothDevice对象,也就是说得到配对的蓝牙适配器
								BluetoothDevice device = (BluetoothDevice) iterator
										.next();
								// 得到远程蓝牙设备的地址
								txt.append("MAC:" + device.getAddress()
										+ "\r\n");
							}
							txt.append("枚举完毕。\r\n");
						}
					} else {
						txt.append("蓝牙打开失败！\r\n");
					}
				} else {
					txt.append("未找到蓝牙设备\r\n");
				}

				// Intent i = new Intent();
				// i.setClass(MainActivity.this, FullscreenActivity.class);
				// startActivity(i);
				// MainActivity.this.finish();
			}

		});

		buSearch.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (adapter != null) {

					txt.append("扫描蓝牙设备...\r\n");
					buMatch.setEnabled(false);

					// 先停止上一次的扫描
					adapter.cancelDiscovery();

					// 扫描周围的可见的蓝牙设备一次要消耗12秒，废电池电量
					// 扫描到了后结果我们怎么接收呢,扫描周围的蓝牙设备每扫描到一个蓝牙设备就会发送一个广播,
					// 我们就需要BroadcastReceiver来接收这个广播,
					// 这个函数是异步的调用,并不是扫描12之后才返回结果的,只要一调用这个函数马上返回,不会等12秒
					adapter.startDiscovery();

					// 创建一个IntentFilter对象,将其action指定为BluetoothDevice.ACTION_FOUND
					// IntentFilter它是一个过滤器,只有符合过滤器的Intent才会被我们的BluetoothReceiver所接收
					IntentFilter intent = new IntentFilter();
					intent.addAction(BluetoothDevice.ACTION_FOUND);
					intent.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
					intent.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
					intent.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
					// 创建一个BluetoothReceiver对象
					bluetoothReceiver = new BluetoothReceiver();
					// 注册广播接收器 注册完后每次发送广播后，BluetoothReceiver就可以接收到这个广播了
					registerReceiver(bluetoothReceiver, intent);

				}
			}
		});

		buMatch.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (foundDevice != null) {

					adapter.cancelDiscovery();

					txt.append("开始配对...\r\n");

					Class<? extends BluetoothDevice> btClass = foundDevice
							.getClass();

					setPin(btClass, foundDevice);
					createBond(btClass, foundDevice);
					// cancelPairingUserInputMethod(btClass, foundDevice);

					txt.append("等待配对结果...\r\n");

				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	boolean setPin(Class<? extends BluetoothDevice> btClass,
			BluetoothDevice btDevice) {

		Boolean returnValue = false;

		txt.append("设置 PIN 码：1234\r\n");

		try {

			Method removeBondMethod = btClass.getDeclaredMethod("setPin",
					new Class[] { byte[].class });

			returnValue = (Boolean) removeBondMethod.invoke(btDevice,
					new Object[] { "1234".getBytes() });

		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	boolean createBond(Class<? extends BluetoothDevice> btClass,
			BluetoothDevice btDevice) {
		Boolean returnValue = false;

		txt.append("开始绑定...\r\n");

		try {

			// create bond
			Method createBondMethod = btClass.getMethod("createBond");
			returnValue = (Boolean) createBondMethod.invoke(btDevice);

		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	boolean cancelPairingUserInputMethod(
			Class<? extends BluetoothDevice> btClass, BluetoothDevice btDevice) {
		Boolean returnValue = false;

		txt.append("取消用户输入...\r\n");

		try {

			// cancelPairingUserInput
			Method cancelPairingUserInputMethod = btClass
					.getMethod("cancelPairingUserInput");
			returnValue = (Boolean) cancelPairingUserInputMethod
					.invoke(btDevice);

		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	boolean connect(BluetoothDevice btDevice) {
		Boolean returnValue = false;
		String dName = btDevice.getName();
		txt.append("开始连接..." + dName);

		UUID uuid = UUID.fromString(SPP_UUID);
		try {
			adapter.cancelDiscovery();

			btSocket = btDevice.createRfcommSocketToServiceRecord(uuid);
			btSocket.connect();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return returnValue;
	}

	// 接收广播
	class BluetoothReceiver extends BroadcastReceiver {

		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			txt.append("action = " + action + "\r\n");

			// 只要BluetoothReceiver接收到来自于系统的广播,这个广播是什么呢,是我找到了一个远程蓝牙设备
			// Intent代表刚刚发现远程蓝牙设备适配器的对象,可以从收到的Intent对象取出一些信息
			BluetoothDevice btDevice = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				txt.append("Got bluetooth: \r\n\tdevice = "
						+ btDevice.getName() + ", MAC = "
						+ btDevice.getAddress() + "\r\n");

				// 找到指定的设备
				if (btDevice.getAddress().equals(MAC_UOPC)) {
					foundDevice = btDevice;
					buMatch.setEnabled(true);
					txt.append("\t找到目标设备：" + btDevice.getAddress() + "\r\n");
				}

			} else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {

				txt.append(btDevice.getName() + "的绑定状态：...");
				int state = btDevice.getBondState();
				switch (state) {
				case BluetoothDevice.BOND_BONDED:
					remoteDevice = btDevice;
					connect(remoteDevice);
					txt.append("BOND_BONDED\r\n");
					break;
				case BluetoothDevice.BOND_BONDING:
					txt.append("BOND_BONDING\r\n");
					break;
				case BluetoothDevice.BOND_NONE:
					txt.append("BOND_NONE\r\n");
					break;
				case BluetoothDevice.ERROR:
					txt.append("ERROR\r\n");
					break;
				}
			} else if ("android.bluetooth.device.action.PAIRING_REQUEST"
					.equals(action)) {

				Class<? extends BluetoothDevice> btClass = btDevice.getClass();
				setPin(btClass, btDevice);
				createBond(btClass, btDevice);
				//cancelPairingUserInputMethod(btClass, btDevice);
			}
		}
	}
}
